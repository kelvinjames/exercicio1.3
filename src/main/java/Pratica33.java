import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        Matriz matriz1 = new Matriz(3, 2);
        Matriz matriz2 = new Matriz(2, 3);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        double[][] m1 = matriz1.getMatriz();
        m1[0][0] = 5.0;
        m1[0][1] = 5.0;
        m1[1][0] = 5.0;
        m1[1][1] = 5.0;
        m1[2][0] = 5.0;
        m1[2][1] = 5.0;
        double[][] m2 = matriz2.getMatriz();
        m2[0][0] = 5.0;
        m2[0][1] = 5.0;
        m2[0][2] = 5.0;
        m2[1][0] = 5.0;
        m2[1][1] = 5.0;
        m2[1][2] = 5.0;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        System.out.println(matriz1);
        System.out.println(matriz2);
        Matriz soma = orig.soma(matriz1);
        Matriz prod = orig.prod(matriz2);
        System.out.println(soma);
        System.out.println(prod);
    }
}
